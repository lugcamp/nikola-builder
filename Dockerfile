
###########################################
# Throwaway image with C compiler installed
FROM python:3.7-alpine as bigimage

# install the C compiler
RUN apk add --no-cache linux-headers g++ libxml2-dev libxslt-dev jpeg-dev

WORKDIR /usr/src/app

COPY requirements.txt .

# instead of installing, create a wheel
RUN pip wheel --wheel-dir=/root/wheels -r requirements.txt

###########################################
# Image WITHOUT C compiler but WITH uWSGI
FROM python:3.7-alpine as smallimage

RUN apk add --no-cache libxml2 libxslt

COPY --from=bigimage /root/wheels /root/wheels

WORKDIR /usr/src/app
COPY requirements.txt .

# Ignore the Python package index
# and look for archives in
# /root/wheels directory
RUN apk add --no-cache git sshpass; pip install \
      --no-index \
      --find-links=/root/wheels  -r requirements.txt
